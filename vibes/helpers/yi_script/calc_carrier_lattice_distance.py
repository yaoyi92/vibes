from ase.io.cube import read_cube
from ase.units import Bohr
from ase.geometry import get_distances
import numpy as np
import typer

app = typer.Typer()


@app.command()
def main(cube_file: str):
    """Calculate the carrier-lattice distance defined in equation (4) in the paper:
        Chenmu Zhang, Ruoyu Wang, Himani Mishra, and Yuanyue Liu
    Phys. Rev. Lett. 130, 087001"""
    cube_dict = read_cube(open(cube_file))

    atoms = cube_dict["atoms"]
    cube_data = cube_dict["data"]
    cube_origin = cube_dict["origin"]
    nx, ny, nz = cube_data.shape
    sum_density = np.sum(cube_data) * atoms.get_volume() / (nx * ny * nz) / (Bohr**3)
    print("The integral of this eigenstate density: ", sum_density)

    # get the positions of voxels
    xgrid = np.linspace(0, 1.0 * (nx - 1) / nx, nx)
    ygrid = np.linspace(0, 1.0 * (ny - 1) / ny, ny)
    zgrid = np.linspace(0, 1.0 * (nz - 1) / nz, nz)
    xv, yv, zv = np.meshgrid(xgrid, ygrid, zgrid, indexing="ij")
    positions = (
        xv[:, :, :, np.newaxis] * atoms.get_cell()[0]
        + yv[:, :, :, np.newaxis] * atoms.get_cell()[1]
        + zv[:, :, :, np.newaxis] * atoms.get_cell()[2]
        + cube_origin
    )

    # calculate the minimum distance of these voxels to the atoms
    distances_min = np.zeros((nx, ny, nz)) + np.finfo(float).max
    for i in range(len(atoms)):
        _, distances = get_distances(
            positions, atoms.get_positions()[i], cell=atoms.get_cell(), pbc=True
        )
        distances = distances.reshape((nx, ny, nz))
        distances_min = np.minimum(distances_min, distances)
    d_carrier_lattice = np.sum((distances_min * cube_data) / np.sum(cube_data))

    print("The calculated carrier-lattice distance (Angstrom): ", d_carrier_lattice)


if __name__ == "__main__":
    app()
